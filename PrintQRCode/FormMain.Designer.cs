﻿
namespace PrintQRCode
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.ppdQRCode = new System.Windows.Forms.PrintPreviewDialog();
            this.lblSalesNo = new System.Windows.Forms.Label();
            this.tlpQRCodeInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lblLastSalesNo = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtxtLog = new System.Windows.Forms.RichTextBox();
            this.tlpSettingsBase = new System.Windows.Forms.TableLayoutPanel();
            this.tctrlConfig = new System.Windows.Forms.TabControl();
            this.tpAutoPrint = new System.Windows.Forms.TabPage();
            this.tlpAutoPrinterControl = new System.Windows.Forms.TableLayoutPanel();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.cbbPrinter = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbMachine = new System.Windows.Forms.ComboBox();
            this.lblMachine = new System.Windows.Forms.Label();
            this.chkCancelPrint = new System.Windows.Forms.CheckBox();
            this.tpPrint = new System.Windows.Forms.TabPage();
            this.tlpPrinterControl = new System.Windows.Forms.TableLayoutPanel();
            this.lblInput = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtSaleNo = new System.Windows.Forms.TextBox();
            this.btnPreview = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtxtTip = new System.Windows.Forms.RichTextBox();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.tlpSettings = new System.Windows.Forms.TableLayoutPanel();
            this.cbbPrinterOutput = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbTMSSys = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tlpBase = new System.Windows.Forms.TableLayoutPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.設定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSettingsSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tlpQRCodeInfo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tlpSettingsBase.SuspendLayout();
            this.tctrlConfig.SuspendLayout();
            this.tpAutoPrint.SuspendLayout();
            this.tlpAutoPrinterControl.SuspendLayout();
            this.tpPrint.SuspendLayout();
            this.tlpPrinterControl.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbSettings.SuspendLayout();
            this.tlpSettings.SuspendLayout();
            this.tlpBase.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ppdQRCode
            // 
            this.ppdQRCode.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.ppdQRCode.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.ppdQRCode.ClientSize = new System.Drawing.Size(400, 300);
            this.ppdQRCode.Enabled = true;
            this.ppdQRCode.Icon = ((System.Drawing.Icon)(resources.GetObject("ppdQRCode.Icon")));
            this.ppdQRCode.Name = "ppdQRCode";
            this.ppdQRCode.UseAntiAlias = true;
            this.ppdQRCode.Visible = false;
            // 
            // lblSalesNo
            // 
            this.lblSalesNo.AutoSize = true;
            this.lblSalesNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSalesNo.Location = new System.Drawing.Point(93, 3);
            this.lblSalesNo.Margin = new System.Windows.Forms.Padding(3);
            this.lblSalesNo.Name = "lblSalesNo";
            this.lblSalesNo.Size = new System.Drawing.Size(186, 15);
            this.lblSalesNo.TabIndex = 4;
            this.lblSalesNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tlpQRCodeInfo
            // 
            this.tlpQRCodeInfo.BackColor = System.Drawing.Color.Transparent;
            this.tlpQRCodeInfo.ColumnCount = 2;
            this.tlpQRCodeInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpQRCodeInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpQRCodeInfo.Controls.Add(this.lblSalesNo, 1, 0);
            this.tlpQRCodeInfo.Controls.Add(this.lblLastSalesNo, 0, 0);
            this.tlpQRCodeInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpQRCodeInfo.Location = new System.Drawing.Point(3, 19);
            this.tlpQRCodeInfo.Name = "tlpQRCodeInfo";
            this.tlpQRCodeInfo.RowCount = 2;
            this.tlpQRCodeInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpQRCodeInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpQRCodeInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpQRCodeInfo.Size = new System.Drawing.Size(282, 42);
            this.tlpQRCodeInfo.TabIndex = 5;
            // 
            // lblLastSalesNo
            // 
            this.lblLastSalesNo.AutoSize = true;
            this.lblLastSalesNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLastSalesNo.Location = new System.Drawing.Point(3, 3);
            this.lblLastSalesNo.Margin = new System.Windows.Forms.Padding(3);
            this.lblLastSalesNo.Name = "lblLastSalesNo";
            this.lblLastSalesNo.Size = new System.Drawing.Size(84, 15);
            this.lblLastSalesNo.TabIndex = 3;
            this.lblLastSalesNo.Text = "銷貨/結帳單號";
            this.lblLastSalesNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.tlpQRCodeInfo);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 303);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(288, 64);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "最後一筆QRCode資訊";
            // 
            // rtxtLog
            // 
            this.rtxtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtLog.Location = new System.Drawing.Point(3, 3);
            this.rtxtLog.Name = "rtxtLog";
            this.rtxtLog.Size = new System.Drawing.Size(702, 699);
            this.rtxtLog.TabIndex = 8;
            this.rtxtLog.Text = "";
            // 
            // tlpSettingsBase
            // 
            this.tlpSettingsBase.ColumnCount = 1;
            this.tlpSettingsBase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSettingsBase.Controls.Add(this.groupBox2, 0, 2);
            this.tlpSettingsBase.Controls.Add(this.tctrlConfig, 0, 1);
            this.tlpSettingsBase.Controls.Add(this.btnPreview, 0, 4);
            this.tlpSettingsBase.Controls.Add(this.groupBox1, 0, 3);
            this.tlpSettingsBase.Controls.Add(this.gbSettings, 0, 0);
            this.tlpSettingsBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSettingsBase.Location = new System.Drawing.Point(711, 3);
            this.tlpSettingsBase.Name = "tlpSettingsBase";
            this.tlpSettingsBase.RowCount = 5;
            this.tlpSettingsBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlpSettingsBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tlpSettingsBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tlpSettingsBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSettingsBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpSettingsBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpSettingsBase.Size = new System.Drawing.Size(294, 699);
            this.tlpSettingsBase.TabIndex = 4;
            // 
            // tctrlConfig
            // 
            this.tctrlConfig.Controls.Add(this.tpAutoPrint);
            this.tctrlConfig.Controls.Add(this.tpPrint);
            this.tctrlConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tctrlConfig.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tctrlConfig.Location = new System.Drawing.Point(3, 103);
            this.tctrlConfig.Name = "tctrlConfig";
            this.tctrlConfig.SelectedIndex = 0;
            this.tctrlConfig.Size = new System.Drawing.Size(288, 194);
            this.tctrlConfig.TabIndex = 9;
            this.tctrlConfig.SelectedIndexChanged += new System.EventHandler(this.tctrlConfig_SelectedIndexChanged);
            // 
            // tpAutoPrint
            // 
            this.tpAutoPrint.Controls.Add(this.tlpAutoPrinterControl);
            this.tpAutoPrint.Location = new System.Drawing.Point(4, 24);
            this.tpAutoPrint.Name = "tpAutoPrint";
            this.tpAutoPrint.Padding = new System.Windows.Forms.Padding(3);
            this.tpAutoPrint.Size = new System.Drawing.Size(280, 166);
            this.tpAutoPrint.TabIndex = 0;
            this.tpAutoPrint.Text = "自動列印";
            this.tpAutoPrint.UseVisualStyleBackColor = true;
            // 
            // tlpAutoPrinterControl
            // 
            this.tlpAutoPrinterControl.BackColor = System.Drawing.Color.Transparent;
            this.tlpAutoPrinterControl.ColumnCount = 2;
            this.tlpAutoPrinterControl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpAutoPrinterControl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAutoPrinterControl.Controls.Add(this.btnStop, 0, 5);
            this.tlpAutoPrinterControl.Controls.Add(this.btnStart, 0, 4);
            this.tlpAutoPrinterControl.Controls.Add(this.cbbPrinter, 1, 1);
            this.tlpAutoPrinterControl.Controls.Add(this.label1, 0, 1);
            this.tlpAutoPrinterControl.Controls.Add(this.cbbMachine, 1, 0);
            this.tlpAutoPrinterControl.Controls.Add(this.lblMachine, 0, 0);
            this.tlpAutoPrinterControl.Controls.Add(this.chkCancelPrint, 1, 2);
            this.tlpAutoPrinterControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAutoPrinterControl.Location = new System.Drawing.Point(3, 3);
            this.tlpAutoPrinterControl.Name = "tlpAutoPrinterControl";
            this.tlpAutoPrinterControl.RowCount = 6;
            this.tlpAutoPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpAutoPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpAutoPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpAutoPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAutoPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpAutoPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpAutoPrinterControl.Size = new System.Drawing.Size(274, 160);
            this.tlpAutoPrinterControl.TabIndex = 8;
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Red;
            this.tlpAutoPrinterControl.SetColumnSpan(this.btnStop, 2);
            this.btnStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Location = new System.Drawing.Point(3, 134);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(268, 23);
            this.btnStop.TabIndex = 12;
            this.btnStop.Text = "停止";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Visible = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.Lime;
            this.tlpAutoPrinterControl.SetColumnSpan(this.btnStart, 2);
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Location = new System.Drawing.Point(3, 105);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(268, 23);
            this.btnStart.TabIndex = 11;
            this.btnStart.Text = "啟動";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cbbPrinter
            // 
            this.cbbPrinter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbPrinter.FormattingEnabled = true;
            this.cbbPrinter.Location = new System.Drawing.Point(76, 32);
            this.cbbPrinter.Name = "cbbPrinter";
            this.cbbPrinter.Size = new System.Drawing.Size(195, 23);
            this.cbbPrinter.Sorted = true;
            this.cbbPrinter.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 23);
            this.label1.TabIndex = 7;
            this.label1.Text = "監控印表機";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbbMachine
            // 
            this.cbbMachine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbMachine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbMachine.FormattingEnabled = true;
            this.cbbMachine.Location = new System.Drawing.Point(76, 3);
            this.cbbMachine.Name = "cbbMachine";
            this.cbbMachine.Size = new System.Drawing.Size(195, 23);
            this.cbbMachine.Sorted = true;
            this.cbbMachine.TabIndex = 6;
            // 
            // lblMachine
            // 
            this.lblMachine.AutoSize = true;
            this.lblMachine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMachine.Location = new System.Drawing.Point(3, 3);
            this.lblMachine.Margin = new System.Windows.Forms.Padding(3);
            this.lblMachine.Name = "lblMachine";
            this.lblMachine.Size = new System.Drawing.Size(67, 23);
            this.lblMachine.TabIndex = 5;
            this.lblMachine.Text = "驗貨人員";
            this.lblMachine.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkCancelPrint
            // 
            this.chkCancelPrint.AutoSize = true;
            this.chkCancelPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkCancelPrint.Location = new System.Drawing.Point(76, 61);
            this.chkCancelPrint.Name = "chkCancelPrint";
            this.chkCancelPrint.Size = new System.Drawing.Size(195, 19);
            this.chkCancelPrint.TabIndex = 13;
            this.chkCancelPrint.Text = "取消列印工作";
            this.chkCancelPrint.UseVisualStyleBackColor = true;
            // 
            // tpPrint
            // 
            this.tpPrint.Controls.Add(this.tlpPrinterControl);
            this.tpPrint.Location = new System.Drawing.Point(4, 24);
            this.tpPrint.Name = "tpPrint";
            this.tpPrint.Padding = new System.Windows.Forms.Padding(3);
            this.tpPrint.Size = new System.Drawing.Size(280, 166);
            this.tpPrint.TabIndex = 1;
            this.tpPrint.Text = "手動列印";
            this.tpPrint.UseVisualStyleBackColor = true;
            // 
            // tlpPrinterControl
            // 
            this.tlpPrinterControl.ColumnCount = 2;
            this.tlpPrinterControl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpPrinterControl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrinterControl.Controls.Add(this.lblInput, 0, 2);
            this.tlpPrinterControl.Controls.Add(this.btnPrint, 1, 3);
            this.tlpPrinterControl.Controls.Add(this.txtSaleNo, 1, 2);
            this.tlpPrinterControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPrinterControl.Location = new System.Drawing.Point(3, 3);
            this.tlpPrinterControl.Name = "tlpPrinterControl";
            this.tlpPrinterControl.RowCount = 5;
            this.tlpPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrinterControl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrinterControl.Size = new System.Drawing.Size(274, 160);
            this.tlpPrinterControl.TabIndex = 7;
            // 
            // lblInput
            // 
            this.lblInput.AutoSize = true;
            this.lblInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInput.Location = new System.Drawing.Point(3, 3);
            this.lblInput.Margin = new System.Windows.Forms.Padding(3);
            this.lblInput.Name = "lblInput";
            this.lblInput.Size = new System.Drawing.Size(84, 23);
            this.lblInput.TabIndex = 8;
            this.lblInput.Text = "銷貨/結帳單號";
            this.lblInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.Lime;
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Location = new System.Drawing.Point(196, 32);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "手動列印";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtSaleNo
            // 
            this.txtSaleNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSaleNo.Location = new System.Drawing.Point(93, 3);
            this.txtSaleNo.Name = "txtSaleNo";
            this.txtSaleNo.Size = new System.Drawing.Size(178, 23);
            this.txtSaleNo.TabIndex = 9;
            // 
            // btnPreview
            // 
            this.btnPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPreview.Location = new System.Drawing.Point(3, 667);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(288, 29);
            this.btnPreview.TabIndex = 10;
            this.btnPreview.Text = "預覽列印";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Visible = false;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.rtxtTip);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 373);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 288);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "說明";
            // 
            // rtxtTip
            // 
            this.rtxtTip.BackColor = System.Drawing.Color.DimGray;
            this.rtxtTip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtTip.Font = new System.Drawing.Font("Microsoft JhengHei UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rtxtTip.ForeColor = System.Drawing.Color.White;
            this.rtxtTip.Location = new System.Drawing.Point(3, 19);
            this.rtxtTip.Name = "rtxtTip";
            this.rtxtTip.ReadOnly = true;
            this.rtxtTip.Size = new System.Drawing.Size(282, 266);
            this.rtxtTip.TabIndex = 12;
            this.rtxtTip.Text = "";
            // 
            // gbSettings
            // 
            this.gbSettings.BackColor = System.Drawing.Color.White;
            this.gbSettings.Controls.Add(this.tlpSettings);
            this.gbSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSettings.Location = new System.Drawing.Point(3, 3);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(288, 94);
            this.gbSettings.TabIndex = 15;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "設定";
            // 
            // tlpSettings
            // 
            this.tlpSettings.BackColor = System.Drawing.Color.Transparent;
            this.tlpSettings.ColumnCount = 2;
            this.tlpSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSettings.Controls.Add(this.cbbPrinterOutput, 1, 1);
            this.tlpSettings.Controls.Add(this.label6, 0, 1);
            this.tlpSettings.Controls.Add(this.cbbTMSSys, 1, 0);
            this.tlpSettings.Controls.Add(this.label8, 0, 0);
            this.tlpSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSettings.Location = new System.Drawing.Point(3, 19);
            this.tlpSettings.Name = "tlpSettings";
            this.tlpSettings.RowCount = 3;
            this.tlpSettings.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpSettings.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSettings.Size = new System.Drawing.Size(282, 72);
            this.tlpSettings.TabIndex = 0;
            // 
            // cbbPrinterOutput
            // 
            this.cbbPrinterOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbPrinterOutput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbPrinterOutput.FormattingEnabled = true;
            this.cbbPrinterOutput.Location = new System.Drawing.Point(101, 32);
            this.cbbPrinterOutput.Name = "cbbPrinterOutput";
            this.cbbPrinterOutput.Size = new System.Drawing.Size(178, 23);
            this.cbbPrinterOutput.Sorted = true;
            this.cbbPrinterOutput.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 23);
            this.label6.TabIndex = 16;
            this.label6.Text = "QRCode印表機";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbbTMSSys
            // 
            this.cbbTMSSys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbTMSSys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTMSSys.FormattingEnabled = true;
            this.cbbTMSSys.Location = new System.Drawing.Point(101, 3);
            this.cbbTMSSys.Name = "cbbTMSSys";
            this.cbbTMSSys.Size = new System.Drawing.Size(178, 23);
            this.cbbTMSSys.Sorted = true;
            this.cbbTMSSys.TabIndex = 15;
            this.cbbTMSSys.SelectedIndexChanged += new System.EventHandler(this.cbbTMSSys_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 3);
            this.label8.Margin = new System.Windows.Forms.Padding(3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 23);
            this.label8.TabIndex = 14;
            this.label8.Text = "TMS系統";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tlpBase
            // 
            this.tlpBase.ColumnCount = 2;
            this.tlpBase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpBase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tlpBase.Controls.Add(this.rtxtLog, 0, 0);
            this.tlpBase.Controls.Add(this.tlpSettingsBase, 1, 0);
            this.tlpBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBase.Location = new System.Drawing.Point(0, 24);
            this.tlpBase.Name = "tlpBase";
            this.tlpBase.RowCount = 1;
            this.tlpBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpBase.Size = new System.Drawing.Size(1008, 705);
            this.tlpBase.TabIndex = 4;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.設定ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 設定ToolStripMenuItem
            // 
            this.設定ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSettingsSave});
            this.設定ToolStripMenuItem.Name = "設定ToolStripMenuItem";
            this.設定ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.設定ToolStripMenuItem.Text = "設定";
            // 
            // tsmiSettingsSave
            // 
            this.tsmiSettingsSave.Name = "tsmiSettingsSave";
            this.tsmiSettingsSave.Size = new System.Drawing.Size(158, 22);
            this.tsmiSettingsSave.Text = "儲存使用者設定";
            this.tsmiSettingsSave.Click += new System.EventHandler(this.tsmiSettingsSave_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.tlpBase);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "保固QRCode列印系統";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.tlpQRCodeInfo.ResumeLayout(false);
            this.tlpQRCodeInfo.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tlpSettingsBase.ResumeLayout(false);
            this.tctrlConfig.ResumeLayout(false);
            this.tpAutoPrint.ResumeLayout(false);
            this.tlpAutoPrinterControl.ResumeLayout(false);
            this.tlpAutoPrinterControl.PerformLayout();
            this.tpPrint.ResumeLayout(false);
            this.tlpPrinterControl.ResumeLayout(false);
            this.tlpPrinterControl.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gbSettings.ResumeLayout(false);
            this.tlpSettings.ResumeLayout(false);
            this.tlpSettings.PerformLayout();
            this.tlpBase.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PrintPreviewDialog ppdQRCode;
        private System.Windows.Forms.Label lblSalesNo;
        private System.Windows.Forms.TableLayoutPanel tlpQRCodeInfo;
        private System.Windows.Forms.Label lblLastSalesNo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtxtLog;
        private System.Windows.Forms.TableLayoutPanel tlpSettingsBase;
        private System.Windows.Forms.TableLayoutPanel tlpBase;
        private System.Windows.Forms.TabControl tctrlConfig;
        private System.Windows.Forms.TabPage tpAutoPrint;
        private System.Windows.Forms.TableLayoutPanel tlpAutoPrinterControl;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ComboBox cbbPrinter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbMachine;
        private System.Windows.Forms.Label lblMachine;
        private System.Windows.Forms.TabPage tpPrint;
        private System.Windows.Forms.TableLayoutPanel tlpPrinterControl;
        private System.Windows.Forms.Label lblInput;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox txtSaleNo;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtxtTip;
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.TableLayoutPanel tlpSettings;
        private System.Windows.Forms.ComboBox cbbTMSSys;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbbPrinterOutput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 設定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiSettingsSave;
        private System.Windows.Forms.CheckBox chkCancelPrint;
    }
}

