﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintQRCode.Controls
{
    /// <summary>
    /// Custom ComboBox Item
    /// </summary>
    internal class ComboBoxItem
    {
        public string Text { get; set; } = string.Empty;
        public string Value { get; set; }= string.Empty;

        public override string ToString()
        {
            return Text;
        }
    }
}
