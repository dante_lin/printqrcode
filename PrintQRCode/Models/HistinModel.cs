﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintQRCode.Models
{
    /// <summary>
    /// 銷貨明細檔
    /// </summary>
    internal class HistinModel
    {
        /// <summary>
        /// 產品料號
        /// </summary>
        public string HICOD { get; set; }

        /// <summary>
        /// 產品名稱
        /// </summary>
        public string HINAM { get; set; }

        /// <summary>
        /// 產品數量
        /// </summary>
        public int HINUM { get; set; }

        /// <summary>
        /// 產品大類
        /// </summary>
        public string ITCOD { get; set; }

        /// <summary>
        /// 產品中類
        /// </summary>
        public string ITCO2 { get; set; }

        /// <summary>
        /// 產品小類
        /// </summary>
        public string ITCO3 { get; set; }
    }
}
