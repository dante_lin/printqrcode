﻿using System;
using System.Collections.Generic;

namespace PrintQRCode.Models
{
    /// <summary>
    /// 銷貨主檔
    /// </summary>
    internal class PoseinModel
    {
        /// <summary>
        /// 銷貨編號
        /// </summary>
        public string PCOD1 { get; set; }

        /// <summary>
        /// 銷貨日期。YYY.MM.DD
        /// </summary>
        public string PDAT1 { get; set; }

        /// <summary>
        /// 銷貨日期。YYYY-MM-DD HH:mm:ss
        /// </summary>
        public DateTime Create_Date { get; set; }

        /// <summary>
        /// 客戶代碼(平台代碼)
        /// </summary>
        public string PPCOD { get; set; }

        /// <summary>
        /// 發票開立。0=隨貨附、1=月底開立、2=不開立
        /// </summary>
        public int POFFE { get; set; }

        /// <summary>
        /// 原始訂單編號
        /// </summary>
        public string PJONO { get; set; }

        /// <summary>
        /// 備註1
        /// </summary>
        public string PBAK1 { get; set; }

        /// <summary>
        /// 分析備註1，取得平台買家ID
        /// </summary>
        /// <returns></returns>
        public string UserID
        {
            get
            {
                const string keyword = "買家ID:";
                if (string.IsNullOrWhiteSpace(this.PBAK1) || !this.PBAK1.Contains(keyword))
                {
                    return string.Empty;
                }
                else
                {
                    int sIdx = this.PBAK1.IndexOf(keyword);
                    int eIdx = this.PBAK1.IndexOf("/", sIdx);
                    if (sIdx == -1 || eIdx == -1 || sIdx >= eIdx)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        string result = this.PBAK1.Substring(sIdx + keyword.Length, eIdx - (sIdx + keyword.Length));
                        return result.ToLowerInvariant();//轉小寫
                    }
                }
            }
        }

        /// <summary>
        /// 銷貨明細
        /// </summary>
        public List<HistinModel> Histins { get; set; }
    }
}
