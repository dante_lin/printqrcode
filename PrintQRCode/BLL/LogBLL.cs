﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintQRCode.BLL
{
    /// <summary>
    /// LOG BLL
    /// </summary>
    internal class LogBLL
    {
        /// <summary>
        /// Log資料夾路徑
        /// </summary>
        private static readonly string logPath = Environment.CurrentDirectory + @"\Logs";

        /// <summary>
        /// 寫入Log
        /// </summary>
        /// <param name="msg">訊息</param>
        public static void WriteLog(string msg)
        {
            try
            {
                string fileName = DateTime.Today.ToString("yyyy-MM-dd") + ".txt";
                string filePath = logPath + @"\" + fileName;
                string nowTime = DateTime.Now.ToString("HH:mm:ss");

                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                if (!File.Exists(filePath))
                {
                    File.Create(filePath).Close();
                }

                using (StreamWriter sw = File.AppendText(filePath))
                {
                    sw.Write($"[{nowTime}] {msg}");
                    sw.WriteLine();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
