﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PrintQRCode.BLL
{
    internal class BaseBLL
    {
        /// <summary>
        /// 民國轉西元
        /// </summary>
        /// <param name="rocDate">民國日期</param>
        /// <returns></returns>
        public static DateTime? ConvertROCToAD(string rocDate)
        {
            if (string.IsNullOrWhiteSpace(rocDate))
            {
                return null;
            }
            else
            {
                rocDate = new Regex(@"[-.\\/ ]").Replace(rocDate, string.Empty);
                if (rocDate.Length != 7)
                {
                    return null;
                }
                else
                {
                    if (!int.TryParse(rocDate.Substring(0, 3), out int y) || !int.TryParse(rocDate.Substring(3, 2), out int m) || !int.TryParse(rocDate.Substring(5, 2), out int d))
                    {
                        return null;
                    }
                    else
                    {
                        return new DateTime(y + 1911, m, d);
                    }
                }
            }
        }
    }
}
