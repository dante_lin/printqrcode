﻿using PrintQRCode.Enums;
using PrintQRCode.Properties;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintQRCode.BLL
{
    internal class GraphicBLL
    {
        #region Singleton Pattern
        public static GraphicBLL GetInstance()
        {
            if (_instance == null)
            {
                _instance = new GraphicBLL();
            }
            return _instance;
        }
        private static GraphicBLL _instance;
        private GraphicBLL()
        {
        }
        #endregion

        /// <summary>
        /// TP809 X位置偏移量，否則會印出紙張外
        /// </summary>
        private const int TP809_OFFSET = 58;

        /// <summary>
        /// 字體
        /// </summary>
        private readonly Font _font = new("Arial", 24F, FontStyle.Bold, GraphicsUnit.Point);

        /// <summary>
        /// Barcode字體
        /// </summary>
        private readonly Font _barcodeFont = new("Arial", 36F, FontStyle.Bold, GraphicsUnit.Pixel);

        /// <summary>
        /// 筆刷
        /// </summary>
        private readonly SolidBrush _solidBrush = new(Color.Black);

        /// <summary>
        /// QRCode背景圖片
        /// </summary>
        private Bitmap _background = Resources.QRCode_Background;

        /// <summary>
        /// QRCode代開圖片
        /// </summary>
        private Bitmap _behalf_background = Resources.QRCode_Behalf;

        private Graphics _g = null;
        private int _xOffset = 0;

        /// <summary>
        /// Y軸位移
        /// </summary>
        private int _yOffset = 0;

        /// <summary>
        /// 紙張中心點
        /// </summary>
        private int _paperCenterX = 0;

        /// <summary>
        /// 畫保固QRCode
        /// </summary>
        /// <param name="g"></param>
        /// <param name="tmsSysType"></param>
        /// <param name="guid"></param>
        /// <param name="behalf">代開發票</param>
        /// <returns></returns>
        public bool DrawWarranty(Graphics g, TMSSysType tmsSysType, string guid, bool behalf = false)
        {
            _g = g;
            #region aws無法連線應急措施
            //this._background = new Bitmap(400, 1);
            //this._yOffset = _background.Height;
            //FixOffset(tmsSysType);
            //return true;
            #endregion
            this._background = behalf ? Resources.QRCode_Background_v2 : Resources.QRCode_Background;
            FixOffset(tmsSysType);
            return DrawBackground() && DrawQRCode(guid);
        }

        /// <summary>
        /// 畫銷貨單號後三碼
        /// </summary>
        /// <param name="g"></param>
        /// <param name="tmsSysType"></param>
        /// <param name="saleNo"></param>
        /// <param name="behalf">代開發票</param>
        /// <returns></returns>
        public bool DrawSaleNo(Graphics g, TMSSysType tmsSysType, string saleNo, bool behalf = false)
        {
            if (!string.IsNullOrWhiteSpace(saleNo) && saleNo.Length >= 3)
            {
                _g = g;
                FixOffset(tmsSysType);
                string salesNoCheck = saleNo.Substring(saleNo.Length - 3, 3);
                SizeF sf = _g.MeasureString(salesNoCheck, _font);
                Point location = new(_xOffset + (int)(_background.Width / 2 - sf.Width / 2), this._yOffset);
                _g.DrawString(salesNoCheck, _font, _solidBrush, location);
                this._yOffset += (int)sf.Height;
                if (tmsSysType == TMSSysType.驗貨 && behalf)//加畫包裝條碼
                {
                    DrawBehalf();
                    DrawPackageBarcode(saleNo);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 重設圖片DPI
        /// </summary>
        /// <param name="filePath"></param>
        public void ResetResolution(string filePath)
        {
            using (Bitmap b = (Bitmap)Image.FromFile(filePath))
            {
                //TP809 DPI = 203
                b.SetResolution(203, 203);
                b.Save(@"C:\Download\203DPI.jpg");
            }
        }

        /// <summary>
        /// 根據系統調整位置
        /// </summary>
        /// <param name="tmsSysType"></param>
        private void FixOffset(TMSSysType tmsSysType)
        {
            //TP809寬=57mm=456
            //1mm=8px
            switch (tmsSysType)
            {
                case TMSSysType.驗貨:
                    _xOffset = TP809_OFFSET + 30;
                    _paperCenterX = 288;
                    break;
                case TMSSysType.POS:
                    _xOffset = 15;
                    _paperCenterX = 210;
                    break;
            }
        }

        /// <summary>
        /// 畫底圖
        /// </summary>
        /// <returns></returns>
        private bool DrawBackground()
        {
            if (_background != null)
            {
                Point location = new(_xOffset, 0);
                _g.DrawImage(_background, location);
                this._yOffset = _background.Height;
                //測試對齊用
                //g.DrawRectangle(Pens.Red, new Rectangle(location.X, location.Y, _background.Width, _background.Height));
                //g.DrawLine(Pens.Red, new Point(location.X + _background.Width / 2, location.Y), new Point(location.X + _background.Width / 2, location.Y + _background.Height));
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 畫QRCode
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        private bool DrawQRCode(string guid)
        {
            using (Bitmap qrCode = CreateQRCodeImage($"https://web.crazywoojin.com/detail/{guid}"))
            {
                if (qrCode != null)
                {
                    qrCode.SetResolution(203, 203);
                    Point location = new((_paperCenterX - qrCode.Width / 2), 208);
                    _g.DrawImage(qrCode, location);
                    //測試對齊用
                    //g.DrawRectangle(Pens.Green, new Rectangle(location.X, location.Y, qrCode.Width, qrCode.Height));
                    //g.DrawLine(Pens.Green, new Point(location.X + qrCode.Width / 2, location.Y), new Point(location.X + qrCode.Width / 2, location.Y + qrCode.Height));
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 畫包裝Barcode
        /// </summary>
        /// <param name="saleNo">銷貨單號</param>
        /// <returns></returns>
        private bool DrawPackageBarcode(string saleNo)
        {
            if (!string.IsNullOrWhiteSpace(saleNo) && long.TryParse(saleNo, out long tmp))
            {
                string barcode = tmp.ToString("X");
                //Barcode
                BarcodeLib.Barcode b = new();
                b.HoritontalResolution = 203;
                b.VerticalResolution = 203;
                b.IncludeLabel = true;
                b.LabelFont = _barcodeFont;
                int w = 340;
                int h = 150;
                Image img = b.Encode(BarcodeLib.TYPE.CODE128, barcode, Color.Black, Color.White, w, h);
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                Point img_location = new(_xOffset + (int)(_background.Width / 2 - img.Width / 2), this._yOffset);
                _g.DrawImage(img, img_location);
                //邊框
                _g.DrawRectangle(new Pen(_solidBrush, 4), new Rectangle(img_location.X - 20, img_location.Y, img.Width + 40, img.Height));
                //文字
                Font font = new("Arial", 10F, FontStyle.Bold, GraphicsUnit.Point);
                StringFormat drawFormat = new StringFormat();
                drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                Point str_location = new Point(img_location.X + img.Width + 20, img_location.Y + 15);
                _g.DrawString("※僅供內部出貨使用※", font, this._solidBrush, str_location, drawFormat);
                this._yOffset += img.Height;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 畫代開發票
        /// </summary>
        /// <returns></returns>
        private bool DrawBehalf()
        {
            //ResetResolution(@"C:\Download\QRCode_Behalf.jpg");
            if (_background != null)
            {
                Point location = new(_xOffset, _yOffset);
                _g.DrawImage(_behalf_background, location);
                this._yOffset += _behalf_background.Height + 20;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 建立QRCode圖片
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private Bitmap CreateQRCodeImage(string data)
        {
            QRCodeGenerator qrGenerator = new();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.M);
            QRCode qrCode = new(qrCodeData);
            return qrCode.GetGraphic(4);//(33*n)*(33*n)
        }
    }
}
