﻿using PrintQRCode.BLL;
using PrintQRCode.Controls;
using PrintQRCode.DAL;
using PrintQRCode.Enums;
using PrintQRCode.Models;
using PrintQRCode.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Linq;
using System.Management;
using System.Threading;
using System.Windows.Forms;

namespace PrintQRCode
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// 列印狀態
        /// </summary>
        enum PrinterStatus
        {
            其他 = 1,
            未知 = 2,
            閒置 = 3,
            列印 = 4,
            預熱 = 5,
            已停止列印 = 6,
            離線 = 7,
        }

        /// <summary>
        /// 委派顯示訊息
        /// </summary>
        /// <param name="msg">訊息</param>
        private delegate void ShowLogDelegate(string msg, bool isError = false);

        /// <summary>
        /// 委派UI更新
        /// </summary>
        /// <param name="text"></param>
        /// <param name="ctrl"></param>
        private delegate void UpdateUIDelegate(string text, Control ctrl);

        /// <summary>
        /// 自動列印監控的迴圈旗標
        /// </summary>
        private bool _run = false;

        /// <summary>
        /// 取消監控印表機的列印工作
        /// </summary>
        private bool _cancelPrint = false;

        /// <summary>
        /// 是否需要保固QRCode旗標
        /// </summary>
        private bool _needWarranty = true;

        /// <summary>
        /// TMS系統
        /// </summary>
        private TMSSysType _tmsSysType = TMSSysType.驗貨;

        /// <summary>
        /// 機台使用者
        /// </summary>
        private string _machine = string.Empty;

        /// <summary>
        /// 銷貨單號
        /// </summary>
        private string _salesNo = string.Empty;

        /// <summary>
        /// 客戶代號
        /// </summary>
        private string _cust = string.Empty;

        /// <summary>
        /// 發票開立
        /// </summary>
        private int _tax_type = 0;

        /// <summary>
        /// 訂單GUID
        /// </summary>
        private string _guid = string.Empty;

        /// <summary>
        /// 自動列印所監控的印表機
        /// </summary>
        private string _printerDeviceSource = string.Empty;

        /// <summary>
        /// 印表機狀態
        /// </summary>
        private PrinterStatus? _printerStatus = null;

        /// <summary>
        /// 列印文件
        /// </summary>
        private PrintDocument _printDocument;

        public FormMain()
        {
            try
            {
                InitializeComponent();
                InitializePrinterList();
                InitializeTMSUsers();
                InitializePrinter();
                InitializeTMSSystem();
                InitializeTip();
                chkCancelPrint.Checked = Settings.Default.CancelPrint;
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
#if DEBUG
            btnPreview.Visible = true;
#endif
            btnStart.PerformClick();
        }

        #region Initialize
        /// <summary>
        /// 初始化印表機列表
        /// </summary>
        private void InitializePrinterList()
        {
            cbbPrinter.Items.Clear();
            cbbPrinterOutput.Items.Clear();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                cbbPrinter.Items.Add(printer);
                cbbPrinterOutput.Items.Add(printer);
            }
            cbbPrinter.SelectedIndex = Settings.Default.Printer;
            cbbPrinterOutput.SelectedIndex = Settings.Default.PrinterOutput;
        }

        /// <summary>
        /// 初始化機台列表
        /// </summary>
        private void InitializeTMSUsers()
        {
            cbbMachine.Items.Clear();
            Dictionary<string, string> tmsUsers = BaseDAL.GetInstance().GetTMSUsers();
            foreach (KeyValuePair<string, string> user in tmsUsers)
            {
                ComboBoxItem item = new()
                {
                    Value = user.Key,
                    Text = user.Value + $"({user.Key})"
                };
                cbbMachine.Items.Add(item);
            }
            cbbMachine.SelectedIndex = Settings.Default.Machine;
        }

        /// <summary>
        /// 初始化TMS系統選項
        /// </summary>
        private void InitializeTMSSystem()
        {
            cbbTMSSys.Items.Clear();
            cbbTMSSys.DataSource = Enum.GetValues(typeof(TMSSysType));
            cbbTMSSys.SelectedIndex = Settings.Default.TMSSys;
        }

        /// <summary>
        /// 初始化印表機
        /// </summary>
        private void InitializePrinter()
        {
            _printDocument = new PrintDocument();
            _printDocument.PrintPage += _printDocument_PrintPage;
            ppdQRCode.Document = _printDocument;
        }

        /// <summary>
        /// 初始化說明
        /// </summary>
        private void InitializeTip()
        {
            rtxtTip.Clear();
            rtxtTip.AppendText("※蝦皮為代開樣式\r\n");
            rtxtTip.AppendText("※商品將排除98、99大類、樣片包\r\n");
            rtxtTip.AppendText("※銷單內無任何產品將不會執行列印\r\n");
            rtxtTip.AppendText("※以下客代不會執行列印：\r\n");
            rtxtTip.AppendText(string.Join('，', BaseDAL.GetInstance().ExcludeCustomer.Select(a => { return "【" + a.value1 + "】" + a.value2; })) + "\r\n");
        }
        #endregion


        /// <summary>
        /// 取得指定印表機狀態
        /// </summary>
        private void GetPrinterStatus()
        {
            string path = @"win32_printer.DeviceId='" + this._printerDeviceSource.ToString() + "'";
            try
            {
                ManagementObject printer = new(path);
                ShowLog($"開始監控印表機 【{this._printerDeviceSource}】");
                int count = 0;
                while (this._run)
                {
                    //釋放資源
                    if (count >= 100)
                    {
                        GC.Collect();
                        count = 0;
                    }
                    #region 取得印表機狀態
                    bool completed = false;
                    int falseCount = 0;
                    do
                    {
                        try
                        {
                            printer.Get();
                            completed = true;
                        }
                        catch (Exception)
                        {
                            falseCount++;
                            Thread.Sleep(100);
                        }
                    } while (!completed && falseCount <= 10);
                    if (!completed && falseCount > 10)
                    {
                        LogBLL.WriteLog("無法取得印表機狀態，關閉自動列印");
                        ShowLog("無法取得印表機狀態，關閉自動列印", true);
                        this._run = false;
                        btnStop_Click(null, null);
                        return;
                    }
                    #endregion
                    PrinterStatus result = (PrinterStatus)Convert.ToInt32(printer.Properties["PrinterStatus"].Value);
                    if (result != this._printerStatus)//狀態異動
                    {
                        if (_cancelPrint)//取消監控印表機列印
                        {
                            CancelPrintJob(this._printerDeviceSource);
                        }
                        ShowLog($"印表機【{result}】");
                        this._printerStatus = result;
                        if (result == PrinterStatus.列印)//發票開始列印
                        {
                            ShowLog($"偵測到{PrinterStatus.列印}");
                            switch (this._tmsSysType)
                            {
                                case TMSSysType.驗貨:
                                    PoseinModel data = BaseDAL.GetInstance().GetLastSaleCheck(this._machine);
                                    this._salesNo = data?.PCOD1 ?? string.Empty;
                                    this._cust = data?.PPCOD ?? string.Empty;
                                    this._tax_type = data?.POFFE ?? 0;
                                    break;
                                case TMSSysType.POS:
                                    this._salesNo = BaseDAL.GetInstance().GetPosLastSale();
                                    this._cust = string.Empty;
                                    this._tax_type = 0;
                                    break;
                            }
                            if (!string.IsNullOrWhiteSpace(this._salesNo))
                            {
                                ShowLog($"目前最後一筆單號：【{this._salesNo}】");
                                this._needWarranty = this._tmsSysType == TMSSysType.POS || BaseDAL.GetInstance().IsNeedWarranty(this._salesNo);
                                if (this._needWarranty)
                                {
                                    //bool? isOrderExists = false;//因AWS停權熱修
                                    bool? isOrderExists = BaseDAL.GetInstance().IsOrderExists(this._tmsSysType, this._salesNo, out this._guid);
                                    if (isOrderExists == false)//訂單資料不存在
                                    {
                                        ShowLog("訂單資料不存在，開始建立訂單資料");
                                        //bool createStatus = true;//因AWS停權熱修
                                        bool createStatus = false;
                                        switch (this._tmsSysType)
                                        {
                                            case TMSSysType.驗貨:
                                                createStatus = BaseDAL.GetInstance().CreateOrder(this._salesNo, out this._guid);
                                                break;
                                            case TMSSysType.POS:
                                                createStatus = BaseDAL.GetInstance().CreatePosOrder(this._salesNo, out this._guid);
                                                break;
                                            default:
                                                return;
                                        }
                                        if (createStatus)
                                        {
                                            ShowLog("訂單資料建立成功，開始自動列印");
                                            this._printDocument.Print();
                                        }
                                        else
                                        {
                                            ShowLog("訂單資料建立失敗，取消自動列印", true);
                                        }
                                    }
                                    else if (isOrderExists == true)
                                    {
                                        ShowLog("該單號已經列印過QRCode，取消自動列印", true);
                                    }
                                    else
                                    {
                                        ShowLog("無法辨識訂單資料是否存在，取消自動列印", true);
                                    }
                                }
                                else
                                {
                                    ShowLog("無須列印保固，開始自動列印");
                                    this._printDocument.Print();
                                }
                            }
                            else
                            {
                                string orderType = "";
                                switch (this._tmsSysType)
                                {
                                    case TMSSysType.驗貨:
                                        orderType = "驗貨的銷貨";
                                        break;
                                    case TMSSysType.POS:
                                        orderType = "面交的結帳";
                                        break;
                                }
                                ShowLog($"找不到最後一筆{orderType}單號，取消自動列印", true);
                            }
                            //更新QRCode資訊
                            UpdateUI(this._salesNo, lblSalesNo);
                        }
                        else
                        {
                            Thread.Sleep(100);
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                    count++;
                }
                ShowLog($"停止監控印表機【{this._printerDeviceSource}】");
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
                ShowLog(ex.ToString(), true);
                this._run = false;
                btnStop_Click(null, null);
            }
        }

        /// <summary>
        /// Cancel the print job. This functions accepts the job number.
        /// An exception will be thrown if access denied.
        /// </summary>
        /// <param name="target">printer name</param>
        /// <returns>bool: true if cancel successfull, else false.</returns>
        /// https://blog.csdn.net/qq_34573534/article/details/103167254
        private bool CancelPrintJob(string target)
        {
            // Variable declarations.
            bool isActionPerformed = false;
            string searchQuery;
            string jobName;
            ManagementObjectSearcher searchPrintJobs;
            ManagementObjectCollection prntJobCollection;
            try
            {
                // Query to get all the queued printer jobs.
                searchQuery = $"SELECT * FROM Win32_PrintJob WHERE Name LIKE '{target}%'";
                // Create an object using the above query.
                searchPrintJobs = new ManagementObjectSearcher(searchQuery);
                // Fire the query to get the collection of the printer jobs.
                prntJobCollection = searchPrintJobs.Get();

                // Look for the job you want to delete/cancel.
                foreach (ManagementObject prntJob in prntJobCollection)
                {
                    jobName = prntJob.Properties["Name"].Value.ToString();
                    string printer = jobName.Split(',')[0];
                    // Job name would be of the format [Printer name], [Job ID]
                    //splitArr = new char[1];
                    //splitArr[0] = Convert.ToChar(",");
                    // Get the job ID.
                    //prntJobID = Convert.ToInt32(jobName.Split(splitArr)[1]);
                    // If the Job Id equals the input job Id, then cancel the job.
                    if (printer == target)
                    {
                        // Performs a action similar to the cancel
                        // operation of windows print console
                        prntJob.Delete();
                        isActionPerformed = true;
                        ShowLog($"已取消【{target}】列印工作");
                        break;
                    }
                }
                return isActionPerformed;
            }
            catch (Exception e)
            {
                // Log the exception.
                ShowLog(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// 顯示Log
        /// </summary>
        /// <param name="message">訊息</param>
        /// <param name="isError">是否為錯誤訊息</param>
        private void ShowLog(string message, bool isError = false)
        {
            if (this.InvokeRequired)//跨執行序
            {
                ShowLogDelegate showMessage = new(ShowLog);
                this.Invoke(showMessage, message, isError);
            }
            else
            {
                string text = $"[{DateTime.Now}] {message}{Environment.NewLine}";
                if (isError)
                {
                    rtxtLog.SelectionStart = rtxtLog.TextLength;
                    rtxtLog.SelectionLength = 0;
                    rtxtLog.SelectionColor = Color.Red;
                    rtxtLog.AppendText(text);
                    rtxtLog.SelectionColor = rtxtLog.ForeColor;
                }
                else
                {
                    rtxtLog.AppendText(text);
                }
            }
        }

        /// <summary>
        /// 更新UI
        /// </summary>
        /// <param name="text">文字</param>
        /// <param name="ctrl">控制項</param>
        private void UpdateUI(string text, Control ctrl)
        {
            if (this.InvokeRequired)
            {
                UpdateUIDelegate delg = new(UpdateUI);
                this.Invoke(delg, text, ctrl);
            }
            else
            {
                ctrl.Text = text;
            }
        }

        #region 事件
        /// <summary>
        /// 啟動監控
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (cbbTMSSys.SelectedIndex == -1)
            {
                MessageBox.Show("請選擇TMS系統", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (cbbPrinterOutput.SelectedIndex == -1)
            {
                MessageBox.Show("請選擇列印QRCode的印表機", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if ((TMSSysType)cbbTMSSys.SelectedItem == TMSSysType.驗貨
                && (cbbMachine.SelectedIndex == -1 || cbbMachine.SelectedItem == null))
            {
                MessageBox.Show("『驗貨』系統，需選擇驗貨人員", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (cbbPrinter.SelectedIndex == -1)
            {
                MessageBox.Show("請選擇列印發票的印表機", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                this._run = true;
                this._tmsSysType = (TMSSysType)cbbTMSSys.SelectedItem;
                switch (this._tmsSysType)
                {
                    case TMSSysType.驗貨:
                        this._machine = ((ComboBoxItem)cbbMachine.SelectedItem).Value;
                        break;
                    case TMSSysType.POS:
                        this._machine = string.Empty;
                        break;
                }
                this._printerDeviceSource = cbbPrinter.Text;
                this._printDocument.PrinterSettings.PrinterName = cbbPrinterOutput.Text;
                this._cancelPrint = chkCancelPrint.Checked;
                ShowLog("開始【" + this._tmsSysType.ToString() + "】系統自動列印");
                Thread thread = new(new ThreadStart(GetPrinterStatus));
                thread.Name = "Monitor";
                thread.IsBackground = true;
                thread.Start();
                btnStart.Visible = false;
                btnStop.Visible = true;
                cbbTMSSys.Enabled = false;
                cbbMachine.Enabled = false;
                cbbPrinter.Enabled = false;
                cbbPrinterOutput.Enabled = false;
                chkCancelPrint.Enabled = false;
                this.BackColor = Color.Lime;
            }
        }

        /// <summary>
        /// 關閉監控
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            this._run = false;
            this._machine = string.Empty;
            this._printerStatus = PrinterStatus.未知;
            ShowLog("結束【" + this._tmsSysType.ToString() + "】系統自動列印");
            lblSalesNo.Text = string.Empty;
            btnStop.Visible = false;
            btnStart.Visible = true;
            cbbTMSSys.Enabled = true;
            cbbPrinterOutput.Enabled = true;
            cbbMachine.Enabled = true;
            cbbPrinter.Enabled = true;
            chkCancelPrint.Enabled = true;
            this.BackColor = Color.Red;
        }

        /// <summary>
        /// 手動列印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cbbPrinterOutput.SelectedIndex == -1 || cbbTMSSys.SelectedIndex == -1 || string.IsNullOrWhiteSpace(txtSaleNo.Text) || this._run)
            {
                return;
            }
            else
            {
                this._tmsSysType = (TMSSysType)cbbTMSSys.SelectedItem;
                this._printDocument.PrinterSettings.PrinterName = cbbPrinterOutput.Text;
                this._salesNo = txtSaleNo.Text;
                switch (this._tmsSysType)
                {
                    case TMSSysType.驗貨:
                        PoseinModel taxData = BaseDAL.GetInstance().GetSalesTax(this._salesNo);
                        this._cust = taxData?.PPCOD ?? string.Empty;
                        this._tax_type = taxData?.POFFE ?? 0;
                        break;
                    case TMSSysType.POS:
                        this._cust = string.Empty;
                        this._tax_type = 0;
                        break;
                    default:
                        return;
                }
                ShowLog($"目前最後一筆單號：【{this._salesNo}】");
                this._needWarranty = this._tmsSysType == TMSSysType.POS || BaseDAL.GetInstance().IsNeedWarranty(this._salesNo);
                if (this._needWarranty)
                {
                    ShowLog("確認訂單資料是否存在");
                    //bool? isOrderExists = true;//因AWS停權熱修
                    bool? isOrderExists = BaseDAL.GetInstance().IsOrderExists(this._tmsSysType, this._salesNo, out this._guid);
                    if (isOrderExists == false)//訂單資料不存在
                    {
                        ShowLog("訂單資料不存在，開始建立訂單資料");
                        bool createStatus = false;
                        switch (this._tmsSysType)
                        {
                            case TMSSysType.驗貨:
                                createStatus = BaseDAL.GetInstance().CreateOrder(this._salesNo, out this._guid);
                                break;
                            case TMSSysType.POS:
                                createStatus = BaseDAL.GetInstance().CreatePosOrder(this._salesNo, out this._guid);
                                break;
                            default:
                                return;
                        }
                        if (createStatus)
                        {
                            ShowLog("訂單資料建立成功，開始手動列印");
                            this._printDocument.Print();
                        }
                        else
                        {
                            ShowLog("訂單資料建立失敗，取消手動列印", true);
                        }
                    }
                    else if (isOrderExists == true)//訂單資料存在
                    {
                        ShowLog("訂單資料已存在，開始手動列印");
                        this._printDocument.Print();
                    }
                    else//無法辨識
                    {
                        ShowLog("無法辨識訂單資料是否存在，取消手動列印", true);
                    }
                }
                else
                {
                    ShowLog("無須列印保固，開始手動列印");
                    this._printDocument.Print();
                }
            }
        }

        /// <summary>
        /// 預覽列印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPreview_Click(object sender, EventArgs e)
        {
            this._tmsSysType = (TMSSysType)cbbTMSSys.SelectedItem;
            this._salesNo = "20012319999";
            this._cust = "S001";
            this._tax_type = 2;
            this._guid = "9f3c65ccee0547429ed6c955acc05e20";
            this._printDocument.PrinterSettings.PrinterName = cbbPrinterOutput.Text;
            ppdQRCode.ShowDialog();
        }

        /// <summary>
        /// 儲存列印設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmiSettingsSave_Click(object sender, EventArgs e)
        {
            Settings.Default.TMSSys = cbbTMSSys.SelectedIndex;
            Settings.Default.PrinterOutput = cbbPrinterOutput.SelectedIndex;
            Settings.Default.Machine = cbbMachine.SelectedIndex;
            Settings.Default.Printer = cbbPrinter.SelectedIndex;
            Settings.Default.CancelPrint = chkCancelPrint.Checked;
            Settings.Default.Save();
            MessageBox.Show("儲存成功", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 列印QRCode事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.PageUnit = GraphicsUnit.Pixel;
            bool cancel = false;
            if (!this._needWarranty)//不須保固
            {
                cancel = true;
            }
            else
            {
                #region 判斷是否代開發票
                bool behalf = false;
                if (this._tax_type == 2 || this._tax_type == 1)//不開立、月底開立發票
                {
                    switch (this._cust)
                    {
                        case "S001"://蝦皮1店
                        case "S003"://蝦皮2店
                        case "S004"://蝦皮3店
                            behalf = true;
                            break;
                        default:
                            behalf = false;
                            break;
                    }
                }
                #endregion
                cancel = cancel ||
                    !GraphicBLL.GetInstance().DrawWarranty(e.Graphics, this._tmsSysType, this._guid, behalf) ||
                    !GraphicBLL.GetInstance().DrawSaleNo(e.Graphics, this._tmsSysType, this._salesNo, behalf);
            }
            e.Cancel = cancel;
        }

        /// <summary>
        /// 自動/手動切換時，關閉自動
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tctrlConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._run)
            {
                btnStop_Click(null, null);
            }
        }

        /// <summary>
        /// 自動TMS系統切換時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbbTMSSys_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((TMSSysType)cbbTMSSys.SelectedItem)
            {
                case TMSSysType.驗貨:
                    lblMachine.Visible = cbbMachine.Visible = true;
                    lblInput.Text = "銷貨單號";
                    lblLastSalesNo.Text = "銷貨單號";
                    break;
                case TMSSysType.POS://POS無驗貨人員
                    cbbMachine.SelectedIndex = -1;
                    lblInput.Text = "結帳單號";
                    lblLastSalesNo.Text = "結帳單號";
                    lblMachine.Visible = cbbMachine.Visible = false;
                    break;
            }
        }
        #endregion
    }
}
