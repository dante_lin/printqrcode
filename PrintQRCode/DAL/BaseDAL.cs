﻿using Dapper;
using MySql.Data.MySqlClient;
using PrintQRCode.BLL;
using PrintQRCode.Enums;
using PrintQRCode.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;

namespace PrintQRCode.DAL
{
    internal class BaseDAL
    {
        #region Singleton Pattern
        public static BaseDAL GetInstance()
        {
            return _instance ?? new BaseDAL();
        }
        private static readonly BaseDAL _instance;
        private BaseDAL()
        {
            this.ExcludeCustomer = GetSysConfigD("exclude_cust");
        }
        #endregion

        /// <summary>
        /// 欲排除的客戶代號
        /// </summary>
        public List<SysConfigDModel> ExcludeCustomer = new List<SysConfigDModel>();

        /// <summary>
        /// 該銷貨單號是否需要列印保固QRCode
        /// </summary>
        /// <param name="saleNo">銷貨單號</param>
        /// <returns></returns>
        public bool IsNeedWarranty(string saleNo)
        {
            if (string.IsNullOrWhiteSpace(saleNo))
            {
                return false;
            }
            string ppcod = "";
            string query = "SELECT TOP(1) PPCOD FROM POSEIN WHERE PCOD1=@PCOD1";
            object param = new { PCOD1 = saleNo };

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMS"].ConnectionString))
                {
                    ppcod = conn.QueryFirstOrDefault<string>(query, param);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            if (string.IsNullOrWhiteSpace(ppcod))
            {
                return false;
            }
            else
            {
                return !ExcludeCustomer.Any(a => a.value1 == ppcod);
            }
        }

        /// <summary>
        /// 取得指定機台最後一筆驗貨的銷貨單號
        /// </summary>
        /// <param name="machineCode">機台代碼</param>
        /// <returns>銷貨單號</returns>
        public PoseinModel GetLastSaleCheck(string machineCode)
        {
            PoseinModel result = new();
            string query = @"
SELECT TOP(1) 
L.PCOD1, M.PPCOD, M.POFFE
FROM PoseinCheckDataLog AS L
JOIN POSEIN AS M ON M.PCOD1=L.PCOD1
WHERE L.SCODE=@SCODE 
ORDER BY L.CREATE_Date DESC"
;
            object param = new { SCODE = machineCode };

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMS"].ConnectionString))
                {
                    result = conn.QueryFirstOrDefault<PoseinModel>(query, param);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return result;
        }

        /// <summary>
        /// 取得POS機最後一筆結帳單號
        /// </summary>
        /// <returns></returns>
        public string GetPosLastSale()
        {
            string result = string.Empty;
            string query = "SELECT TOP(1) RCOD1 FROM PosMainTemp ORDER BY CREATE_Date DESC";

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMS"].ConnectionString))
                {
                    result = conn.QueryFirstOrDefault<string>(query);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return result;
        }

        /// <summary>
        /// 取得設定檔
        /// </summary>
        /// <param name="_key">關鍵字</param>
        /// <returns>設定</returns>
        public List<SysConfigDModel> GetSysConfigD(string _key)
        {
            #region aws無法連線應急措施
            //return new List<SysConfigDModel> {//因AWS停權熱修
            //new SysConfigDModel{ value1="C010",value2="保固" },
            //new SysConfigDModel{ value1="F001",value2="外包設計師" }
            //};
            #endregion
            List<SysConfigDModel> result = new List<SysConfigDModel>();
            string query = @"
SELECT 
D.value1, D.value2, D.value3 
FROM sys_config_m AS M 
JOIN sys_config_d AS D ON D.m_id=M.id 
WHERE M._key=@_key";
            object param = new { _key = _key };

            MySqlConnection conn = null;
            try
            {
                using (conn = new(ConfigurationManager.ConnectionStrings["CWJ"].ConnectionString))
                {
                    result = conn.Query<SysConfigDModel>(query, param).ToList();
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
                result = null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// 取得TMS使用者清單
        /// </summary>
        /// <returns>TMS使用者清單</returns>
        public Dictionary<string, string> GetTMSUsers()
        {
            Dictionary<string, string> result = new();
            string query = "SELECT SCODE, SNAME FROM SALE";

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMS"].ConnectionString))
                {
                    result = conn.Query(query).ToDictionary(row => (string)row.SCODE, row => (string)row.SNAME);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// 訂單資料是否已經存在
        /// </summary>
        /// <param name="sysType">系統(0=驗貨，1=POS)</param>
        /// <param name="sale_no">銷貨單號</param>
        /// <param name="guid">GUID</param>
        /// <returns></returns>
        public bool? IsOrderExists(TMSSysType sysType, string sale_no, out string guid)
        {
            guid = string.Empty;
            bool? result = null;
            if (string.IsNullOrWhiteSpace(sale_no))
            {
                return result;
            }
            string checkQuery = string.Empty;
            switch (sysType)
            {
                case TMSSysType.驗貨:
                    checkQuery = @"SELECT guid FROM order_m WHERE id=@id";
                    break;
                case TMSSysType.POS:
                    checkQuery = @"SELECT guid FROM pos_order_m WHERE id=@id";
                    break;
            }
            object checkParam = new { id = sale_no };

            MySqlConnection conn = null;
            try
            {
                using (conn = new(ConfigurationManager.ConnectionStrings["CWJ"].ConnectionString))
                {
                    guid = conn.ExecuteScalar<string>(checkQuery, checkParam);
                    result = !string.IsNullOrWhiteSpace(guid);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
                result = null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// 建立訂單資料
        /// </summary>
        /// <param name="sale_no">銷貨單號</param>
        /// <param name="guid">GUID</param>
        /// <returns></returns>
        public bool CreateOrder(string sale_no, out string guid)
        {
            bool result = false;
            guid = Guid.NewGuid().ToString("N");
            PoseinModel salesData = GetSalesData(sale_no);
            if (salesData == null || salesData.Histins == null || salesData.Histins.Count == 0)//找不到銷貨資料
            {
                return false;
            }
            else
            {
                #region 排除指定客戶代碼
                if (this.ExcludeCustomer != null && this.ExcludeCustomer.Any(a => a.value1 == salesData.PPCOD))
                {
                    return false;
                }
                #endregion

                #region 主檔
                string mQuery = @"
INSERT INTO 
order_m 
(id, guid, user_id, original_no, sale_src, sale_date) 
VALUES
(@id, @guid, @user_id, @original_no, @sale_src, @sale_date)
";
                object mParam = new
                {
                    id = sale_no,
                    guid = guid,
                    user_id = salesData.UserID,
                    original_no = salesData.PJONO,
                    sale_src = salesData.PPCOD,
                    sale_date = BaseBLL.ConvertROCToAD(salesData.PDAT1)
                };
                #endregion

                #region 明細檔
                string dQuery = @"
INSERT INTO 
order_d 
(m_id, item_no, item_name, item_qty, item_kind, item_kind2, item_kind3) 
VALUES
(@m_id, @item_no, @item_name, @item_qty, @item_kind, @item_kind2, @item_kind3) 
";
                List<object> dParam = new List<object>();
                foreach (HistinModel histin in salesData.Histins)
                {
                    dParam.Add(new
                    {
                        m_id = sale_no,
                        item_no = histin.HICOD,
                        item_name = histin.HINAM,
                        item_qty = histin.HINUM,
                        item_kind = histin.ITCOD,
                        item_kind2 = histin.ITCO2,
                        item_kind3 = histin.ITCO3,
                    });
                }
                #endregion

                MySqlConnection conn = null;
                try
                {
                    using (conn = new(ConfigurationManager.ConnectionStrings["CWJ"].ConnectionString))
                    using (TransactionScope trans = new())
                    {
                        result = conn.Execute(mQuery, mParam) > 0 && conn.Execute(dQuery, dParam) > 0;
                        if (result)
                        {
                            trans.Complete();
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogBLL.WriteLog(ex.ToString());
                    result = false;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 建立POS訂單資料
        /// </summary>
        /// <param name="sale_no">銷貨單號</param>
        /// <param name="guid">GUID</param>
        /// <returns></returns>
        public bool CreatePosOrder(string sale_no, out string guid)
        {
            bool result = false;
            guid = Guid.NewGuid().ToString("N");
            PoseinModel salesData = GetPosSalesData(sale_no);
            if (salesData == null || salesData.Histins == null || salesData.Histins.Count == 0)//找不到銷貨資料
            {
                return false;
            }
            else
            {
                #region 主檔
                string mQuery = @"
INSERT INTO 
pos_order_m 
(id, guid, sale_date) 
VALUES
(@id, @guid, @sale_date)
";
                object mParam = new
                {
                    id = sale_no,
                    guid = guid,
                    sale_date = salesData.Create_Date
                };
                #endregion

                #region 明細檔
                string dQuery = @"
INSERT INTO 
pos_order_d 
(m_id, item_no, item_name, item_qty, item_kind, item_kind2, item_kind3) 
VALUES
(@m_id, @item_no, @item_name, @item_qty, @item_kind, @item_kind2, @item_kind3) 
";
                List<object> dParam = new List<object>();
                foreach (HistinModel histin in salesData.Histins)
                {
                    dParam.Add(new
                    {
                        m_id = sale_no,
                        item_no = histin.HICOD,
                        item_name = histin.HINAM,
                        item_qty = histin.HINUM,
                        item_kind = histin.ITCOD,
                        item_kind2 = histin.ITCO2,
                        item_kind3 = histin.ITCO3,
                    });
                }
                #endregion

                MySqlConnection conn = null;
                try
                {
                    using (conn = new(ConfigurationManager.ConnectionStrings["CWJ"].ConnectionString))
                    using (TransactionScope trans = new())
                    {
                        result = conn.Execute(mQuery, mParam) > 0 && conn.Execute(dQuery, dParam) > 0;
                        if (result)
                        {
                            trans.Complete();
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogBLL.WriteLog(ex.ToString());
                    result = false;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 取得銷貨資料
        /// </summary>
        /// <param name="sale_no">銷貨單號</param>
        /// <returns></returns>
        public PoseinModel GetSalesData(string sale_no)
        {
            PoseinModel result = null;
            string query = @"
SELECT
M.PCOD1, M.PDAT1, M.PPCOD, M.PJONO, M.PBAK1, D.HICOD, D.HINAM, D.HINUM, I.ITCOD, I1.ITCO2, I1.ITCO3
FROM POSEIN AS M
JOIN HISTIN AS D ON D.HCOD1=M.PCOD1 AND D.HINUM IS NOT NULL
JOIN ITEM AS I ON I.ICODE=D.HICOD
JOIN ITEM1 AS I1 ON I1.ICODE1=D.HICOD
WHERE (D.HYCOD NOT IN ('98','99')
OR D.HICOD BETWEEN '99010014001' AND '99010014004')
AND M.PCOD1 = @PCOD1
";
            object param = new { PCOD1 = sale_no };

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMS"].ConnectionString))
                {
                    conn.Query<PoseinModel, HistinModel, PoseinModel>(query, (m, d) =>
                    {
                        if (result == null)
                        {
                            result = m;
                        }
                        if (result.Histins == null)
                        {
                            result.Histins = new List<HistinModel>();
                        }
                        result.Histins.Add(d);
                        return m;
                    }, splitOn: "HICOD", param: param);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return result;
        }

        /// <summary>
        /// 取得POS銷貨資料
        /// </summary>
        /// <param name="sale_no">POS結帳單號</param>
        /// <returns></returns>
        public PoseinModel GetPosSalesData(string sale_no)
        {
            PoseinModel result = null;
            string query = @"
SELECT 
M.RCOD1 AS PCOD1, M.CREATE_Date, D.ICODE AS HICOD, D.INAME AS HINAM, D.Quantity AS HINUM, I.ITCOD, I1.ITCO2, I1.ITCO3
FROM PosMainTemp AS M
JOIN PosDetailsTemp AS D ON D.MainID=M.MainID
JOIN ITEM AS I ON I.ICODE=D.ICODE
JOIN ITEM1 AS I1 ON I1.ICODE1=D.ICODE
WHERE (I.ITCOD NOT IN ('98','99') OR D.ICODE BETWEEN '99010014001' AND '99010014004')
AND M.RCOD1 = @RCOD1
";
            object param = new { RCOD1 = sale_no };

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMS"].ConnectionString))
                {
                    conn.Query<PoseinModel, HistinModel, PoseinModel>(query, (m, d) =>
                    {
                        if (result == null)
                        {
                            result = m;
                        }
                        if (result.Histins == null)
                        {
                            result.Histins = new List<HistinModel>();
                        }
                        result.Histins.Add(d);
                        return m;
                    }, splitOn: "HICOD", param: param);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return result;
        }

        /// <summary>
        /// 取得銷貨開立發票資料
        /// </summary>
        /// <param name="sale_no">銷貨單號</param>
        /// <returns></returns>
        public PoseinModel GetSalesTax(string sale_no)
        {
            PoseinModel result = null;
            string query = @"
SELECT
PPCOD, POFFE
FROM POSEIN
WHERE PCOD1 = @PCOD1
";
            object param = new { PCOD1 = sale_no };

            SqlConnection conn = null;
            try
            {
                using (conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMS"].ConnectionString))
                {
                    result = conn.QueryFirstOrDefault<PoseinModel>(query, param);
                }
            }
            catch (Exception ex)
            {
                LogBLL.WriteLog(ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return result;
        }
    }
}
