﻿namespace PrintQRCode.Enums
{
    /// <summary>
    /// TMS系統
    /// </summary>
    public enum TMSSysType
    {
        驗貨 = 0,
        POS = 1,
    }
}
